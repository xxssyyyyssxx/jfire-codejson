package com.jfireframework.codejson.function.impl.read.wrapper;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import com.jfireframework.codejson.function.JsonReader;

public class BigDecimalReader implements JsonReader
{
	
	@Override
	public Object read(Type entityType, Object value)
	{
		return new BigDecimal((String) value);
	}
	
}
