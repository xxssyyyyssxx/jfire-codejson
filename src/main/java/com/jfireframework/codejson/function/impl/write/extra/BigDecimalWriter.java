package com.jfireframework.codejson.function.impl.write.extra;

import java.math.BigDecimal;
import com.jfireframework.baseutil.collection.StringCache;
import com.jfireframework.codejson.function.impl.write.WriterAdapter;
import com.jfireframework.codejson.tracker.Tracker;

public class BigDecimalWriter extends WriterAdapter
{
	@Override
	public void write(Object field, StringCache cache, Object entity, Tracker tracker)
	{
		BigDecimal bigDecimal = (BigDecimal) field;
		cache.append(bigDecimal.doubleValue());
	}
	
}
