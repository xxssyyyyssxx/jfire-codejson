package com.jfireframework.codejson.methodinfo.impl.write.array;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.WriteStrategy;

public class ReturnArrayIterableMethodInfo extends AbstractWriteArrayMethodInfo
{
    
    public ReturnArrayIterableMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
    }
    
    @Override
    protected void writeOneDim(Class<?> rootType, String bk)
    {
        if (strategy == null)
        {
            str += bk + "if(array1[i1]!=null)\r\n";
            str += bk + "{\r\n";
            str += bk + "\tcache.append('[');\r\n";
            str += bk + "\tObject valueTmp = null;\r\n";
            str += bk + "\tIterator it = ((Iterable)array1[i1]).iterator();\r\n";
            str += bk + "\twhile(it.hasNext())\r\n";
            str += bk + "\t{\r\n";
            str += bk + "\t\tif((valueTmp=it.next())!=null)\r\n";
            str += bk + "\t\t{\r\n";
            str += bk + "\t\t\tif(valueTmp instanceof String)\r\n";
            str += bk + "\t\t\t{\r\n";
            str += bk + "\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
            str += bk + "\t\t\t}\r\n";
            str += bk + "\t\t\telse\r\n";
            str += bk + "\t\t\t{\r\n";
            str += bk + "\t\t\t\tWriterContext.write(valueTmp,cache);\r\n";
            str += bk + "\t\t\t}\r\n";
            str += bk + "\t\t\tcache.append(',');\r\n";
            str += bk + "\t\t}\r\n";
            str += bk + "\t}\r\n";
            str += bk + "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
            str += bk + "cache.append(\"],\");\r\n";
            str += bk + "}\r\n";
        }
        else
        {
            if (strategy.isUseTracker())
            {
                str += bk + "_$tracker.reset(_$reIndexarray1);\r\n";
                str += bk + "int _$reIndexarray0 = _$tracker.indexOf(array1[i1]);\r\n";
                str += bk + "if(_$reIndexarray0 != -1)\r\n";
                str += bk + "{\r\n";
                str += bk + "\tJsonWriter writerarray0 = writeStrategy.getTrackerType(array1[i1].getClass());\r\n";
                str += bk + "\tif(writerarray0 != null)\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\twriterarray0.write(array1[i1],cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t}\r\n";
                str += bk + "\telse\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$reIndexarray0)).append('\"').append('}');\r\n";
                str += bk + "\t}\r\n";
                str += bk + "}\r\n";
                str += bk + "else\r\n";
                str += bk + "{\r\n";
                str += bk + "\t_$reIndexarray0 = _$tracker.put(array1[i1],\"[\"+i1+']',true);\r\n";
                str += bk + "\tcache.append('[');\r\n";
                str += bk + "\tObject valueTmp = null;\r\n";
                str += bk + "\tIterator it = ((Iterable)array1[i1]).iterator();\r\n";
                str += bk + "\tint count = 0;\r\n";
                str += bk + "\twhile(it.hasNext())\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\tif((valueTmp=it.next())!=null)\r\n";
                str += bk + "\t\t{\r\n";
                str += bk + "\t\t\tif(valueTmp instanceof String)\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\telse\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\t_$tracker.reset(_$reIndexarray0);\r\n";
                str += bk + "\t\t\t\tint _$indexarray0 = _$tracker.indexOf(valueTmp);\r\n";
                str += bk + "\t\t\t\tif(_$indexarray0 != -1)\r\n";
                str += bk + "\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\tJsonWriter writerarray0_1 = writeStrategy.getTrackerType(valueTmp.getClass());\r\n";
                str += bk + "\t\t\t\t\tif(writerarray0_1 != null)\r\n";
                str += bk + "\t\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\t\twriterarray0_1.write(valueTmp,cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t\t\t\t\t}\r\n";
                str += bk + "\t\t\t\t\telse\r\n";
                str += bk + "\t\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$indexarray0)).append('\"').append('}');\r\n";
                str += bk + "\t\t\t\t\t}\r\n";
                str += bk + "\t\t\t\t}\r\n";
                str += bk + "\t\t\t\telse\r\n";
                str += bk + "\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\t_$tracker.put(valueTmp,\"[\"+count+']',true);\r\n";
                str += bk + "\t\t\t\t\twriteStrategy.getWriter(valueTmp.getClass()).write(valueTmp,cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t\t\t\t}\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\tcache.append(',');\r\n";
                str += bk + "\t\t\tcount+=1;\r\n";
                str += bk + "\t\t}\r\n";
                str += bk + "\t}\r\n";
                str += bk + "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                str += bk + "\tcache.append(\"],\");\r\n";
                str += bk + "}\r\n";
            }
            else
            {
                str += bk + "if(array1[i1]!=null)\r\n";
                str += bk + "{\r\n";
                str += bk + "\tcache.append('[');\r\n";
                str += bk + "\tObject valueTmp = null;\r\n";
                str += bk + "\tIterator it = ((Iterable)array1[i1]).iterator();\r\n";
                str += bk + "\twhile(it.hasNext())\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\tif((valueTmp=it.next())!=null)\r\n";
                str += bk + "\t\t{\r\n";
                str += bk + "\t\t\tif(valueTmp instanceof String)\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\telse\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\twriteStrategy.getWriter(valueTmp.getClass()).write(valueTmp,cache," + entityName + ",null);\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\tcache.append(',');\r\n";
                str += bk + "\t\t}\r\n";
                str += bk + "\t}\r\n";
                str += bk + "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
                str += bk + "cache.append(\"],\");\r\n";
                str += bk + "}\r\n";
            }
            
        }
    }
    
}
