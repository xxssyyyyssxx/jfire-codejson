package com.jfireframework.codejson.methodinfo.impl.read;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.ReadStrategy;

public class SetBaseMethodInfo extends AbstractReadMethodInfo
{
    
    public SetBaseMethodInfo(Method method, ReadStrategy strategy)
    {
        super(method, strategy);
        String jsonGetMethodName = "get" + getParamType().getName().substring(0, 1).toUpperCase() + getParamType().getName().substring(1);
        str = "if(json.contains(\"" + fieldName + "\"))\r\n";
        str += "{\r\n";
        if (strategy != null && (strategy.containsStrategyField(strategyFieldName) || strategy.containsStrategyType(getParamType())))
        {
            if (getParamType().equals(char.class))
            {
                if (strategy.containsStrategyField(strategyFieldName))
                {
                    str += "\tCharacter c = " + "(Character)readStrategy.getReaderByField(\"" + strategyFieldName + "\").read(" + getParamType().getName() + ".class,json.get(\"" + fieldName + "\"));\r\n";
                }
                else
                {
                    str += "\tCharacter c = " + "(Character)readStrategy.getReader(" + getParamType() + ".class).read(" + getParamType().getName() + ".class,json.get(\"" + fieldName + "\"));\r\n";
                }
            }
            else if (getParamType().equals(boolean.class))
            {
                if (strategy.containsStrategyField(strategyFieldName))
                {
                    str += "\tBoolean b = " + "(Boolean)readStrategy.getReaderByField(\"" + strategyFieldName + "\").read(" + getParamType().getName() + ".class,json.get(\"" + fieldName + "\"));\r\n";
                }
                else
                {
                    str += "\tBoolean b = " + "(Boolean)readStrategy.getReader($sig[0]).read(" + getParamType().getName() + ".class,json.get(\"" + fieldName + "\"));\r\n";
                }
            }
            else
            {
                if (strategy.containsStrategyField(strategyFieldName))
                {
                    str += "\tNumber num = " + "(Number)readStrategy.getReaderByField(\"" + strategyFieldName + "\").read(" + getParamType().getName() + ".class,json.get(\"" + fieldName + "\"));\r\n";
                }
                else
                {
                    str += "\tNumber num = " + "(Number)readStrategy.getReader(" + getParamType().getName() + ".class).read(" + getParamType().getName() + ".class,json.get(\"" + fieldName + "\"));\r\n";
                }
            }
            if (getParamType().equals(int.class))
            {
                str += "\t" + entityName + method.getName() + "(num.intValue());\r\n";
            }
            else if (getParamType().equals(short.class))
            {
                str += "\t" + entityName + method.getName() + "(num.shortValue());\r\n";
            }
            else if (getParamType().equals(long.class))
            {
                str += "\t" + entityName + method.getName() + "(num.LongValue());\r\n";
            }
            else if (getParamType().equals(float.class))
            {
                str += "\t" + entityName + method.getName() + "(num.floatValue());\r\n";
            }
            else if (getParamType().equals(double.class))
            {
                str += "\t" + entityName + method.getName() + "(num.doubleValue());\r\n";
            }
            else if (getParamType().equals(byte.class))
            {
                str += "\t" + entityName + method.getName() + "(num.byteValue());\r\n";
            }
            else if (getParamType().equals(boolean.class))
            {
                str += "\t" + entityName + method.getName() + "(b.booleanValue());\r\n";
            }
            else if (getParamType().equals(Character.class))
            {
                str += "\t" + entityName + method.getName() + "(c.charValue());\r\n";
                
            }
        }
        else
        {
            str += "\t" + entityName + method.getName() + "(json." + jsonGetMethodName + "(\"" + fieldName + "\"));\r\n";
        }
        str += "}\r\n";
    }
    
}
