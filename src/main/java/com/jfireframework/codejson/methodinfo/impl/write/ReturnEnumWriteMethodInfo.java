package com.jfireframework.codejson.methodinfo.impl.write;

import java.lang.reflect.Method;
import com.jfireframework.baseutil.smc.SmcHelper;
import com.jfireframework.codejson.function.WriteStrategy;
import com.jfireframework.codejson.util.NameTool;

public class ReturnEnumWriteMethodInfo extends AbstractWriteMethodInfo
{
    
    public ReturnEnumWriteMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
        Class<?> returnType = method.getReturnType();
        String fieldName = NameTool.getNameFromMethod(method, strategy);
        str = "" + SmcHelper.getTypeName(returnType) + " " + fieldName + " = " + getValue + ";\r\n";
        str += "if(" + fieldName + "!=null)\r\n{\r\n";
        str += "\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
        String key = method.getDeclaringClass().getName() + '.' + fieldName;
        if (strategy == null)
        {
            str += "\tcache.append('\"').append(" + getValue + ".name()).append('\"').append(',');\r\n";
        }
        else
        {
            if (strategy.containsStrategyField(key))
            {
                str += "\twriteStrategy.getWriterByField(\"" + key + "\").write(" + getValue + ",cache," + entityName + ");\r\n";
                str += "\tcache.append(',');\r\n";
            }
            else if (strategy.containsStrategyType(returnType))
            {
                str += "\twriteStrategy.getWriter(" + returnType.getName() + ".class).write(" + getValue + ",cache," + entityName + ");\r\n";
                str += "\tcache.append(',');\r\n";
            }
            else
            {
                if (strategy.isWriteEnumName())
                {
                    str += "\tcache.append('\"').append(" + getValue + ".name()).append('\"').append(',');\r\n";
                }
                else
                {
                    str += "\tcache.append(" + getValue + ".ordinal()).append(',');\r\n";
                }
            }
        }
        str += "}\r\n";
    }
    
}
