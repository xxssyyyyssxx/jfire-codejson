package com.jfireframework.codejson.methodinfo.impl.write.array;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.WriteStrategy;

public class ReturnArrayMapMethodInfo extends AbstractWriteArrayMethodInfo
{
    
    public ReturnArrayMapMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
    }
    
    @Override
    protected void writeOneDim(Class<?> rootType, String bk)
    {
        if (strategy == null)
        {
            str += bk + "if(array1[i1]!=null)\r\n";
            str += bk + "{\r\n";
            str += bk + "\tcache.append('{');\r\n";
            str += bk + "\tIterator it = ((Map)array1[i1]).entrySet().iterator();\r\n";
            str += bk + "\tjava.util.Map.Entry entry = null;\r\n";
            str += bk + "\twhile(it.hasNext())\r\n";
            str += bk + "\t{\r\n";
            str += bk + "\t\tentry =(Entry) it.next();\r\n";
            str += bk + "\t\tif(entry.getKey()!=null && entry.getValue()!=null)\r\n";
            str += bk + "\t\t{\r\n";
            str += bk + "\t\t\tif(entry.getKey() instanceof String)\r\n";
            str += bk + "\t\t\t{\r\n";
            str += bk + "\t\t\t\tcache.append('\\\"').append((String)entry.getKey()).append(\"\\\":\");\r\n";
            str += bk + "\t\t\t}\r\n";
            str += bk + "\t\t\telse\r\n";
            str += bk + "\t\t\t{\r\n";
            str += bk + "\t\t\t\tWriterContext.write(entry.getKey(),cache);\r\n";
            str += bk + "\t\t\t}\r\n";
            str += bk + "\t\t\tif(entry.getValue() instanceof String)\r\n";
            str += bk + "\t\t\t{\r\n";
            str += bk + "\t\t\t\tcache.append('\\\"').append((String)entry.getValue()).append('\\\"');\r\n";
            str += bk + "\t\t\t}\r\n";
            str += bk + "\t\t\telse\r\n";
            str += bk + "\t\t\t{\r\n";
            str += bk + "\t\t\t\tWriterContext.write(entry.getValue(),cache);\r\n";
            str += bk + "\t\t\t}\r\n";
            str += bk + "\t\t\tcache.append(',');\r\n";
            str += bk + "\t\t}\r\n";
            str += bk + "\t}\r\n";
            str += bk + "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
            str += bk + "\tcache.append(\"},\");\r\n";
            str += bk + "}\r\n";
        }
        else
        {
            if (strategy.isUseTracker())
            {
                str += bk + "_$tracker.reset(_$reIndexarray1);\r\n";
                str += bk + "int _$reIndexarray0 = _$tracker.indexOf(array1[i1]);\r\n";
                str += bk + "if(_$reIndexarray0 != -1)\r\n";
                str += bk + "{\r\n";
                str += bk + "\tJsonWriter writerarray0 = writeStrategy.getTrackerType(array1[i1].getClass());\r\n";
                str += bk + "\tif(writerarray0 != null)\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\twriterarray0.write(array1[i1],cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t}\r\n";
                str += bk + "\telse\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$reIndexarray0)).append('\"').append('}');\r\n";
                str += bk + "\t}\r\n";
                str += bk + "}\r\n";
                str += bk + "else\r\n";
                str += bk + "{\r\n";
                str += bk + "\t_$reIndexarray0 = _$tracker.put(array1[i1],\"[\"+i1+']',true);\r\n";
                str += bk + "\tcache.append('{');\r\n";
                str += bk + "\tIterator it = ((Map)array1[i1]).entrySet().iterator();\r\n";
                str += bk + "\tjava.util.Map.Entry entry = null;\r\n";
                str += bk + "\twhile(it.hasNext())\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\tentry = (Entry)it.next();\r\n";
                str += bk + "\t\tif(entry.getKey()!=null && entry.getValue()!=null)\r\n";
                str += bk + "\t\t{\r\n";
                str += bk + "\t\t\tif(entry.getKey() instanceof String)\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\tcache.append('\\\"').append((String)entry.getKey()).append(\"\\\":\");\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\telse\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\twriteStrategy.getWriter(entry.getKey().getClass()).write(entry.getKey(),cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\tif(entry.getValue() instanceof String)\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\tcache.append('\\\"').append((String)entry.getValue()).append('\\\"');\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\telse\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\t_$tracker.reset(_$reIndexarray0);\r\n";
                str += bk + "\t\t\t\tint _$indexarray0 = _$tracker.indexOf(entry.getValue());\r\n\n";
                str += bk + "\t\t\t\tif(_$indexarray0 != -1)\r\n";
                str += bk + "\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\tJsonWriter writerarray0_1 = writeStrategy.getTrackerType(entry.getValue().getClass());\r\n";
                str += bk + "\t\t\t\t\tif(writerarray0_1 != null)\r\n";
                str += bk + "\t\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\t\twriterarray0_1.write(entry.getValue(),cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t\t\t\t\t}\r\n";
                str += bk + "\t\t\t\t\telse\r\n";
                str += bk + "\t\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$indexarray0)).append('\"').append('}');\r\n";
                str += bk + "\t\t\t\t\t}\r\n";
                str += bk + "\t\t\t\t}\r\n";
                str += bk + "\t\t\t\telse\r\n";
                str += bk + "\t\t\t\t{\r\n";
                str += bk + "\t\t\t\t\t_$tracker.put(entry.getValue(),entry.getKey().toString(),false);\r\n";
                str += bk + "\t\t\t\t\twriteStrategy.getWriter(entry.getValue().getClass()).write(entry.getValue(),cache," + entityName + ",_$tracker);\r\n";
                str += bk + "\t\t\t\t}\r\n";
                str += bk + "\t\t\t\tcache.append(',');\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t}\r\n";
                str += bk + "\t}\r\n";
                str += bk + "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                str += bk + "\tcache.append(\"},\");\r\n";
                str += bk + "}\r\n";
            }
            else
            {
                str += bk + "if(array1[i1]!=null)\r\n";
                str += bk + "{\r\n";
                str += bk + "\tcache.append('{');\r\n";
                str += bk + "\tIterator it = ((Map)array1[i1]).entrySet().iterator();\r\n";
                str += bk + "\tjava.util.Map.Entry entry = null;\r\n";
                str += bk + "\twhile(it.hasNext())\r\n";
                str += bk + "\t{\r\n";
                str += bk + "\t\tentry = (Entry)it.next();\r\n";
                str += bk + "\t\tif(entry.getKey()!=null && entry.getValue()!=null)\r\n";
                str += bk + "\t\t{\r\n";
                str += bk + "\t\t\tif(entry.getKey() instanceof String)\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\tcache.append('\\\"').append((String)entry.getKey()).append(\"\\\":\");\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\telse\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\twriteStrategy.getWriter(entry.getKey().getClass()).write(entry.getKey(),cache," + entityName + ",null);\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\tif(entry.getValue() instanceof String)\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\tcache.append('\\\"').append((String)entry.getValue()).append('\\\"');\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\telse\r\n";
                str += bk + "\t\t\t{\r\n";
                str += bk + "\t\t\t\twriteStrategy.getWriter(entry.getValue().getClass()).write(entry.getValue(),cache," + entityName + ",null);\r\n";
                str += bk + "\t\t\t}\r\n";
                str += bk + "\t\t\tcache.append(',');\r\n";
                str += bk + "\t\t}\r\n";
                str += bk + "\t}\r\n";
                str += bk + "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
                str += bk + "cache.append(\"},\");\r\n";
                str += bk + "}\r\n";
            }
        }
    }
    
}
