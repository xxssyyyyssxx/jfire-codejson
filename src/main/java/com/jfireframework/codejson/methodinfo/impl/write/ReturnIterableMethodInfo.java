package com.jfireframework.codejson.methodinfo.impl.write;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.WriteStrategy;
import com.jfireframework.codejson.function.impl.write.wrapper.StringWriter;
import com.jfireframework.codejson.util.NameTool;

/**
 * 如果方法的返回对象实现了Iterable接口，则使用该方法返回热编译代码
 * 
 */
public class ReturnIterableMethodInfo extends AbstractWriteMethodInfo
{
    public ReturnIterableMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
        String fieldName = NameTool.getNameFromMethod(method, strategy);
        str = "Iterable " + fieldName + " = " + getValue + ";\r\n";
        str += "if(" + fieldName + "!=null)\r\n{\r\n";
        String key = method.getDeclaringClass().getName() + '.' + fieldName;
        if (strategy != null)
        {
            if (strategy.containsStrategyField(key))
            {
                // 由于在这里不能确定到底序列化的内容是什么，所以只输出到:号为止
                str += "\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
                str += "\tJsonWriter writer = writeStrategy.getWriterByField(\"" + key + "\");\r\n";
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\t_$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",(Tracker)$4);\r\n";
                }
                else
                {
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",null);\r\n";
                }
                str += "\tcache.append(',');\r\n";
                str += "}\r\n";
            }
            else
            {
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index != -1)\r\n";
                    str += "\t{\r\n";
                    str += "\t\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
                    str += "\t\tJsonWriter writer = writeStrategy.getTrackerType(" + fieldName + ".getClass());\r\n";
                    str += "\t\tif(writer != null)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\twriter.write(" + fieldName + ",cache," + entityName + ",(Tracker)$4);\r\n";
                    str += "\t\t}\r\n";
                    str += "\t\telse\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index)).append('\"').append('}');\r\n";
                    str += "\t\t}\r\n";
                    str += "\telse\r\n";
                    str += "\t{\r\n";
                    str += "\t\tint _$reIndex1 = _$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\t\tcache.append(\"\\\"" + fieldName + "\\\":[\");\r\n";
                    str += "\t\tint count = 0;\r\n";
                    str += "\t\tIterator it =" + fieldName + ".iterator();\r\n";
                    str += "\t\tObject valueTmp = null;\r\n";
                    str += "\t\twhile(it.hasNext())\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tif((valueTmp=it.next())!=null)\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\tif(valueTmp instanceof String || valueTmp instanceof Number || valueTmp instanceof Boolean)\r\n";
                    str += "\t\t\t\t{\r\n";
                    if (strategy.getWriter(String.class) instanceof StringWriter)
                    {
                        str += "\t\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
                    }
                    else
                    {
                        str += "\t\t\t\t\twriteStrategy.getWriter(String.class).write((String)valueTmp,cache," + entityName + ",_$tracker);\r\n";
                    }
                    str += "\t\t\t\t\tcount+=1;\r\n";
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\telse\r\n";
                    str += "\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t_$tracker.reset(_$reIndex1);\r\n";
                    str += "\t\t\t\t\tint _$index1 = _$tracker.indexOf(valueTmp);\r\n";
                    str += "\t\t\t\t\tif(_$index1 != -1)\r\n";
                    str += "\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\tJsonWriter writer = writeStrategy.getTrackerType(valueTmp.getClass());\r\n";
                    str += "\t\t\t\t\t\tif(writer != null)\r\n";
                    str += "\t\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t\twriter.write(valueTmp,cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\t\telse\r\n";
                    str += "\t\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index1)).append('\"').append('}');\r\n";
                    str += "\t\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\t\tcount+=1;\r\n";
                    str += "\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\telse\r\n";
                    str += "\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t_$tracker.put(valueTmp,\"[\"+count+\"]\",true);\r\n";
                    str += "\t\t\t\t\t\twriteStrategy.getWriter(valueTmp.getClass()).write(valueTmp,cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\t\tcount+=1;\r\n";
                    str += "\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t}\r\n";
                    str += "\t\t}\r\n";
                    str += "\t\tcache.append(',');\r\n";
                    str += "\t}\r\n";
                    str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += "\tcache.append(\"],\");\r\n";
                    str += "}\r\n";
                }
                else
                {
                    str += "\tcache.append(\"\\\"" + fieldName + "\\\":[\");\r\n";
                    str += "\tIterator it =" + fieldName + ".iterator();\r\n";
                    str += "\tObject valueTmp = null;\r\n";
                    str += "\twhile(it.hasNext())\r\n\t{\r\n";
                    str += "\t\tif((valueTmp=it.next())!=null)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tif(valueTmp instanceof String)\r\n";
                    str += "\t\t\t{\r\n";
                    if (strategy.getWriter(String.class) instanceof StringWriter)
                    {
                        str += "\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
                    }
                    else
                    {
                        str += "\t\t\t\twriteStrategy.getWriter(String.class).write((String)valueTmp,cache," + entityName + ",null);\r\n";
                    }
                    str += "\t\t\t}\r\n";
                    str += "\t\t\telse\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\twriteStrategy.getWriter(valueTmp.getClass()).write(valueTmp,cache," + entityName + ",null);\r\n";
                    str += "\t\t\t}\r\n";
                    str += "\t\t\tcache.append(',');\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += "\tcache.append(\"],\");\r\n";
                    str += "}\r\n";
                }
            }
        }
        else
        {
            str += "\tcache.append(\"\\\"" + fieldName + "\\\":[\");\r\n";
            str += "\tIterator it =" + fieldName + ".iterator();\r\n";
            str += "\tObject valueTmp = null;\r\n";
            str += "\twhile(it.hasNext())\r\n\t{\r\n";
            str += "\t\tif((valueTmp=it.next())!=null)\r\n";
            str += "\t\t{\r\n";
            str += "\t\t\tif(valueTmp instanceof String)\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\telse\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tWriterContext.write(valueTmp,cache);\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\tcache.append(',');\r\n";
            str += "\t\t}\r\n";
            str += "\t}\r\n";
            str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
            str += "\tcache.append(\"],\");\r\n";
            str += "}\r\n";
        }
    }
    
}
